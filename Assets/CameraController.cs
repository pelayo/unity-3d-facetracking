﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenCvSharp;

public class CameraController : MonoBehaviour {

     // Video parameters
    public MeshRenderer WebCamTextureRenderer;
    public MeshRenderer ProcessedTextureRenderer;
    public int deviceNumber;
    private WebCamTexture _webcamTexture;
    private WebCamTexture mainWebCamTexture;
    private Texture2D processedWebCamTexture;

    // Video size
    private const int imWidth = 640;
    private const int imHeight = 480;
    private int imFrameRate;

    // OpenCVSharp parameters
    private Mat videoSourceImage;
    private Mat greyImage;
    private Texture2D processedTexture;
    private Vec3b[] videoSourceImageData;
    private byte[] greyImageData;

    private CascadeClassifier faceClassifier;
    private CascadeClassifier mouthClassifier;

    // Frame rate parameter
    private int updateFrameCount = 0;
    private int textureCount = 0;
    private int displayCount = 0;

    // Logic
    private bool isFirstTimeRecording = false;


    // Use this for initialization
    private void Start () {

        WebCamDevice[] devices = WebCamTexture.devices;
        Debug.Log("Number of web cams connected: " + devices.Length);

        if(devices.Length > 0) {

            Renderer rend = this.GetComponentInChildren<Renderer>();

            mainWebCamTexture = new WebCamTexture();
            string camName = devices[0].name;
            Debug.Log("The webcam name is " + camName);
            mainWebCamTexture.deviceName = camName;
            mainWebCamTexture.Play();

            // create and assign modified webcam video texture as Texture2D object
            processedWebCamTexture = new Texture2D(imWidth, imHeight, TextureFormat.RGBA32, true, true);
            rend.material.mainTexture = processedWebCamTexture;
            
             // initialize video / image with given size
            videoSourceImage = new Mat(imHeight, imWidth, MatType.CV_8UC3);
            videoSourceImageData = new Vec3b[imHeight * imWidth];
            greyImage = new Mat(imHeight, imWidth, MatType.CV_8UC1);
            greyImageData = new byte[imHeight * imWidth];

            // create and assign processed video texture as Texture2D object
            processedTexture = new Texture2D(imWidth, imHeight, TextureFormat.RGBA32, true, true);
            // rend.material.mainTexture = processedTexture;

        }

        // create opencv window to display the debug information
        Cv2.NamedWindow("Debug Window");

        // Init CV detectors
        InitDetectors();

    }

    private void InitDetectors ()
    {
        
        // Init Detectors

        var faceClassifierFilepath = Application.streamingAssetsPath + "/frontalface_default.xml";
        faceClassifier = new CascadeClassifier(faceClassifierFilepath);
        if (faceClassifier.Empty()) {
            Debug.LogError ("Error loading Face Cascade file.");
        }

        var mouthClassifierFilepath = Application.streamingAssetsPath + "/cascadefina.xml";
        mouthClassifier = new CascadeClassifier(mouthClassifierFilepath);
        if (mouthClassifier.Empty()) {
            Debug.LogError ("Error loading  Cascade file.");
        }

    }
	
	// Update is called once per frame
	private void Update () {
		
        updateFrameCount++;

        if (mainWebCamTexture.isPlaying) {

            if (mainWebCamTexture.didUpdateThisFrame) {

                textureCount++;

                // convert texture of original video to OpenCVSharp Mat object
                TextureToMat();
                 // perform the detection
                ProcessImage(videoSourceImage);
                // update the opencv window of source video
                UpdateWindow(greyImage);
                // convert the OpenCVSharp Mat of grey image to Texture2D
                MatToTexture();

                // Apply efect to texture

                Color32[] pixels;
                pixels = new Color32[mainWebCamTexture.width * mainWebCamTexture.height];
                mainWebCamTexture.GetPixels32(pixels);

                processedWebCamTexture.SetPixels32(pixels);
                processedWebCamTexture.Apply();

            }

        }
        else {

            Debug.Log("Can't find camera!");

        }

        // output frame rate information
        if (updateFrameCount % 30 == 0) {
            //Debug.Log("Frame count: " + updateFrameCount + ", Texture count: " + textureCount + ", Display count: " + displayCount);
        }

	}

    // Convert Unity Texture2D object to OpenCVSharp Mat object
    private void TextureToMat() {

        // Get pixels faster
        Color32[] pixels;
        pixels = new Color32[mainWebCamTexture.width * mainWebCamTexture.height];
        mainWebCamTexture.GetPixels32(pixels);
       
        // Loop through the image pixels
        for (var i = 0; i < imHeight; i++) {
            for (var j = 0; j < imWidth; j++) {
                var col = pixels[j + i * imWidth];
                var vec3 = new Vec3b {
                    Item0 = col.b,
                    Item1 = col.g,
                    Item2 = col.r
                };
                // Set pixel to an array
                videoSourceImageData[j + i * imWidth] = vec3;
            }
        }
        
        // Assign the Vec3b array to Mat
        videoSourceImage.SetArray(0, 0, videoSourceImageData);

    }

    // Convert OpenCVSharp Mat object to Unity Texture2D object
    private void MatToTexture() {
        // greyImageData is byte array, because grey image is grayscale
        greyImage.GetArray(0, 0, greyImageData);
        // create Color32 array that can be assigned to Texture2D directly
        Color32[] c = new Color32[imHeight * imWidth];

        // loop through the image pixels
        for (var i = 0; i < imHeight; i++) {
            for (var j = 0; j < imWidth; j++) {
                byte vec = greyImageData[j + i * imWidth];
                var color32 = new Color32 {
                    r = vec,
                    g = vec,
                    b = vec,
                    a = 0
                };
                c[j + i * imWidth] = color32;
            }
        };

        processedTexture.SetPixels32(c);
        // to update the texture, OpenGL manner
        processedTexture.Apply();
    }

    // Simple detetection
    private void ProcessImage(Mat _image) {

        Cv2.Flip(_image, greyImage, FlipMode.X);
        Cv2.CvtColor(greyImage, greyImage, ColorConversionCodes.BGRA2GRAY);
        Cv2.EqualizeHist(greyImage, greyImage);

        var faces = faceClassifier.DetectMultiScale(
                image: greyImage,
                scaleFactor: 1.1,
                minNeighbors: 3,
                flags: HaarDetectionType.DoRoughSearch | HaarDetectionType.ScaleImage,
                minSize: new OpenCvSharp.Size(100, 100),
                maxSize: new OpenCvSharp.Size ()
                );
        // Debug.Log("Detected faces: " + faces.Length);

        var color = Scalar.FromRgb(255, 255, 0);

        foreach (var faceRect in faces)
        {


            var detectedFaceImage = new Mat(greyImage, faceRect);
            Cv2.Rectangle(greyImage, faceRect, color, 2);

            var nestedObjects = mouthClassifier.DetectMultiScale(
                    image: detectedFaceImage,
                    scaleFactor: 1.13,
                    minNeighbors: 3,
                    flags: HaarDetectionType.DoRoughSearch | HaarDetectionType.ScaleImage,
                    minSize: new Size(10, 20),
                    maxSize: new Size ()
                    );
            // Debug.Log("Detected mouth: " + nestedObjects.Length);

            var mouthCenter = new Point(0,faceRect.Height/1.5);
            var isMouthOpen = false;
            int mouthRadius = 0;

            foreach (var nestedObject in nestedObjects)
            {

                    var center = new Point
                    {
                        X = (int)(Math.Round(nestedObject.X + nestedObject.Width * 0.5, MidpointRounding.ToEven) + faceRect.Left),
                        Y = (int)(Math.Round(nestedObject.Y + nestedObject.Height * 0.5, MidpointRounding.ToEven) + faceRect.Top)
                    };
                   
                    if(center.Y > mouthCenter.Y) {
                       mouthCenter = center;
                       mouthRadius = (int)(Math.Round(nestedObject.Height * 1.0));
                       isMouthOpen = true;
                    }

            }

            GameObject Cam = GameObject.Find("Main Camera");
            GlitchEffect fx =  Cam.GetComponent<GlitchEffect> ();

            if(isMouthOpen) {
            
                Cv2.Circle(greyImage, mouthCenter, (int)mouthRadius , color, thickness: 3);
                fx.startEffect(Remap(mouthRadius,0,30,0,1));
                if(!fx.isRecording && !isFirstTimeRecording) {
                    fx.startRecordingGIF();
                    isFirstTimeRecording = true;
                }

            } else {

                if(isFirstTimeRecording)
                    isFirstTimeRecording = false;
                fx.stopEffect();

            }
        
        }
        

    }

    // Display the debug information in CV window
    private void UpdateWindow(Mat _image) {
        Cv2.ImShow("Debug Window", _image);
        displayCount++;
    }
    
    // close the opencv window
    public void OnDestroy() {
        Cv2.DestroyAllWindows();

    }

    // Utils

    private float Remap (float value, float from1, float to1, float from2, float to2) {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }


}
