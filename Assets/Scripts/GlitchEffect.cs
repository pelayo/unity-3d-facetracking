using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;
using Moments;

#if UNITY_EDITOR
[ExecuteInEditMode] 
[AddComponentMenu ("GlitchEffect")]
#endif
public class GlitchEffect : MonoBehaviour {

	public Texture2D displacementMap;
	float glitchup, glitchdown, flicker,
	glitchupTime = 0.05f, glitchdownTime = 0.05f, flickerTime = 0.5f;

    [Header("Glitch Intensity")]

    [Range(0, 1)]
    public float intensity;
    
    [Range(0, 1)]
    public float flipIntensity;

    [Range(0, 1)]
    public float colorIntensity;

    private Material material;


    // Gif Recording

    private Recorder m_Recorder;
    public bool isRecording;
    private bool isSaving;

    // Get the Shader from the library
 	private void Awake ()
 	{
 		
 		material = new Material( Shader.Find("Hidden/GlitchShader") );


 		m_Recorder = GetComponent<Recorder>();
 		m_Recorder.OnPreProcessingDone = OnProcessingDone;
		//m_Recorder.OnFileSaveProgress = OnFileSaveProgress;
		m_Recorder.OnFileSaved = OnFileSaved;

 	}


     // Apply the effect to the camera
	private void OnRenderImage (RenderTexture source, RenderTexture destination) {

		material.SetFloat("_Intensity", intensity);
        material.SetFloat("_ColorIntensity", colorIntensity);
		material.SetTexture("_DispTex", displacementMap);
        
        flicker += Time.deltaTime * colorIntensity;
        if (flicker > flickerTime){
			material.SetFloat("filterRadius", Random.Range(-3f, 3f) * colorIntensity);
            material.SetVector("direction", Quaternion.AngleAxis(Random.Range(0, 360) * colorIntensity, Vector3.forward) * Vector4.one);
            flicker = 0;
			flickerTime = Random.value;
		}

        if (colorIntensity == 0)
            material.SetFloat("filterRadius", 0);
        
        glitchup += Time.deltaTime * flipIntensity;
        if (glitchup > glitchupTime){
			if(Random.value < 0.1f * flipIntensity)
				material.SetFloat("flip_up", Random.Range(0, 1f) * flipIntensity);
			else
				material.SetFloat("flip_up", 0);
			
			glitchup = 0;
			glitchupTime = Random.value/10f;
		}

        if (flipIntensity == 0)
            material.SetFloat("flip_up", 0);


        glitchdown += Time.deltaTime * flipIntensity;
        if (glitchdown > glitchdownTime){
			if(Random.value < 0.1f * flipIntensity)
				material.SetFloat("flip_down", 1 - Random.Range(0, 1f) * flipIntensity);
			else
				material.SetFloat("flip_down", 1);
			
			glitchdown = 0;
			glitchdownTime = Random.value/10f;
		}

        if (flipIntensity == 0)
            material.SetFloat("flip_down", 1);

        if (Random.value < 0.05 * intensity){
			material.SetFloat("displace", Random.value * intensity);
			material.SetFloat("scale", 1 - Random.value * intensity);
        }
        else
			material.SetFloat("displace", 0);
		
		Graphics.Blit (source, destination, material);

	}

	// Activate the effect
	public void startEffect(float _intensity) {

		intensity = _intensity;
		colorIntensity = _intensity;
		flipIntensity = _intensity;

	}

	// Deactivate the effect
	public void stopEffect() {

		intensity = 0;
		colorIntensity = 0;
		flipIntensity = 0;

	}

	public void startRecordingGIF() {

		m_Recorder.Record();
		isRecording = true;
		isSaving = false;
		Invoke("saveGIF", 3);
		Debug.Log("GIF generation: Start recording.");

	}

	private void saveGIF() {

		if(!isSaving) {
			Debug.Log("GIF generation: Saving File....");
			m_Recorder.Pause();
			m_Recorder.Save();
		}
	
	}


	private void OnProcessingDone()
	{

		Debug.Log("GIF generation: Save done.");
	
	}

	/*
	private void OnFileSaveProgress(int id, float percent)
	{
	}
	*/

	private void OnFileSaved(int id, string filepath)
	{
		
		// Our file has successfully been compressed & written to disk !
		Debug.Log("GIF generation: File Saved.");
		Debug.Log(filepath);
		isRecording = false;
		isSaving = false;


	}


}